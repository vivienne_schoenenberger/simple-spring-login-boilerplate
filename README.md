# A Simple Spring Login
This is a boilerplate login made with spring security
## Pages
- Login/Logout Page "/login"
- Landing Page "/"
- User Page "/home"
- Admin Page "/admin"
## Security
- Bcrypt Password Encoder
## Database 
- H2 Integrated DB


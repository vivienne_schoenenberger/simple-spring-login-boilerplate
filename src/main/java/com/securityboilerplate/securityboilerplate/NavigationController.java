package com.securityboilerplate.securityboilerplate;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
public class NavigationController {
    @GetMapping("/home")
    public String home(){
        return "user";
    }
    @GetMapping("/admin")
    public String admin(){
        return "admin";
    }
    @GetMapping("/")
    public String landing(){
        return "landing";
    }

    @GetMapping("/login")
    String login() {
        return "login";
    }
    @PostMapping("/logout")
    String logout() {
        return "landing";
    }

}

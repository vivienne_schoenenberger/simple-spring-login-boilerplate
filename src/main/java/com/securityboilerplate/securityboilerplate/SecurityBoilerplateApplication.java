package com.securityboilerplate.securityboilerplate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecurityBoilerplateApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecurityBoilerplateApplication.class, args);
	}

}
